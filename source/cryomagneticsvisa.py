'''
CryomagneticsVISA defines the cryomagnetics 4G supply interface functions.
It is primarily concerned around interfacing via the ethernet socket interface. 

'''

from datetime import datetime
import traceback
import socket
import time


class Cryomagnetics4G():

    writeterminationcharacter = bytes('\r',"ascii")
    readterminationcharacter = bytes('\n',"ascii")
    configuration = 'ethernet'
    IPAddress = 'localhost'
    IPPort = 4444
    SerialPort = '/dev/tty_Cryomagnetics4G'
    GPIBAddress = 3
    maxcurrent = 100
    heaterdelay = 30
    querycharacter = '?'
    debug = True
    localIPPort = 1214
    commandDelay = .1
    

    def __init__(self):

        print(str(datetime.now())+ ": Initializing Cryomagnetics4G Magnet Supply.")
        if self.configuration == 'ethernet':

            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.connect((self.IPAddress,self.IPPort))
            self.sock.settimeout(1)
            
            # great, the socket is bound, and will always bind to the same localport. this handles certain cases in the past where an instrument "locks" onto a given IP and port.
            
            self.SetErrorResponseMode(False)
        else:
            raise Exception

    def get_ip_address(self):
        # This is a pretty baller way to get the IP of an interface.
        # Taken from https://stackoverflow.com/questions/24196932/how-can-i-get-the-ip-address-from-nic-in-python
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        return s.getsockname()[0]

    def BoundInput(self,value,low_value=0, high_value=0):

        if value>high_value:
            
            return high_value
        
        elif value<low_value:
            
            return low_value

        else:
            return value

    def IntegerCasting(self,value,length):

        # casts to int and truncates length of integer.
        # example - a 3 digit allowed integer must be between 999 and 0.
        # thus, 0<= value <10^3.
        # generalized 0=< value < 10^n for length n.

        intermediate = int(value)

        if intermediate <0:
            
            return 0

        elif intermediate >= 10**(length):

            return int((10**length) -1)

        else:

            return intermediate


    def GPIBWrite(self,value):
        pass

    def RS232Write(self,value):
        pass

    def EthernetWrite(self,value):

        # build the write string
        toSend = bytearray()
        bytearraystringvalue = bytes(value,"ascii")
        toSend.extend(bytearraystringvalue)
        toSend.extend(self.writeterminationcharacter)        
        self.sock.sendall(toSend)
        time.sleep(self.commandDelay)

        # check if it's a query value. if so, do a read. if not, return Command.

        if not value.find(self.querycharacter)>-1:

            if self.debug:

                print(str(datetime.now())+": "+ toSend.decode())
            
            return ['Command']

        else:
            
            #TODO: perform some sort of error handling here if this times out or pukes.
            try:
                
                recMessage = self.sock.recv(1024).decode()
             
                recMessage = recMessage[:-2] #strip the return and newline chars.

            except:
                recMessage = ''

            if self.debug:

                print(str(datetime.now())+": "+value+"->"+recMessage)

            return recMessage



    def WriteString(self,value):

    ## Write the string and read back until the correct newline character is found.

        if self.configuration=='GPIB':

            return self.GPIBWrite(value)

        elif self.configuration=="RS232":

            return self.RS232Write(value)

        else:

            return self.EthernetWrite(value)



    def SelectModule(self,value=1):
        
        '''
        CHAN 
        Set the module for subsequent remote commands 
        Availability:                   Remote Mode                  
        Command Syntax:CHAN [module number] Example:                       CHAN                       2                       
        Default Parameter:       none       
        Parameter Range:  1 to 2 Description: The CHAN command  selects  the  module  for  subsequent  remote  commands.   
        A  command error is returned if only one module is installed. 
        Related Commands:   CHAN? 
        '''
        boundinputvalue = self.IntegerCasting(self.BoundInput(value,1,2),1)

        self.WriteString('CHAN ' + str(boundinputvalue))

    def QueryModule(self):

        '''
        CHAN?
        Query the currently selected module  
        Availability:                   Always                   
        Command Syntax:       CHAN?       
        Response:<currently selected module> 
        Response Example: 2 
        Description:    The CHAN?  query  returns  the  power  module  currently  selected  for  remote  commands.  
        It returns 1 or 2.  A command error is returned if only one module is installed. 
        Related Commands:    CHAN
        '''
        self.WriteString('CHAN?')
        
    def SetErrorResponseMode(self,value):
       
        '''
        ERROR
        Set error response mode for USB interface 
        Availability:                   Remote                   Mode                   
        Command Syntax:ERROR <Error Mode> 
        Example:                       ERROR                       1                       
        Parameter Range:0 or 1    (0 - disable error reporting, 1 - enable error reporting) 
        Description:  The ERROR command enables or disables error messages when the USB interface is used.  
        It is much easier to handle errors under program control when using the USB interface if error messages are disabled, 
        but it is desirable to enable error messages if a terminal program is used to interactively control and query the 4G. 
        Related Commands:  ERROR? 
        '''
        if value:
            value = 1

        else:
            value = 0
        
        boundinputvalue = self.IntegerCasting(self.BoundInput(value,0,1),1)

        self.WriteString('ERROR ' + str(boundinputvalue))

    def QueryErrorResponseMode(self):

        '''
        ERROR?
        Query error response mode 
        Availability:                   Always                   
        Command Syntax:       ERROR?                  
        Response:                    <Error                    Mode>                    
        Response Example: 0  
        Response Range:  0 or 1 
        Description:  The ERROR? query returns the selected error reporting mode. 
        Related Commands:   ERROR 
        '''
        self.WriteString('ERROR?')

    def SetMagnetPersistentCurrent(self,value,shim=False,fieldmode=False):
        '''
        IMAG
        Sets the magnet current (or magnetic field strength).   
        Availability:                   Remote                   Mode                   
        Command Syntax:       IMAG       [value]       
        IMAG <shim_id> <value>   (shim mode only) 
        Example:                       IMAG                       47.1123                       
        IMAG X2 -13.4571  (shim mode only) 
        Default Parameter:       0.0       
        Parameter  Range:+/-Maximum  Magnet  Current
        Description: The IMAG command sets the magnet current shown on the display.  The supply must be  in  standby  or  a  command  error  will  be  returned.    
        The  value  must  be  supplied  in  the  selected  units  - amperes or field (kG).  
        If Shim Mode is enabled, the persistent mode current displayed for the named shim is set if the shim parameter is provided. 
        Related Commands:    IMAG?
        '''


        #TODO: Add shim mode functionality

        if shim and fieldmode:

            pass

        elif shim and not fieldmode:

            pass

        elif fieldmode and not shim:

            pass

        else:

            #TODO: handle field/current mode correctly.

            boundinputvalue = self.BoundInput(value,0,self.maxcurrent)

            self.WriteString('IMAG ' + str(boundinputvalue))


    def QueryMagnetCurrent(self,shimmode=False,):

        '''
        IMAG?
        Query magnet current  (or magnetic field strength) 
        Availability:                   Always                  
        Command Syntax:       IMAG?       
        Response:<Magnet Current> <Units> 
        Response Example: 87.9350 A 
        Description:    The IMAG?  query  returns  the  magnet  current  (or  magnetic  field  strength)  in  the  present units.   
        If the persistent switch heater is ON the magnet current returned will be the same as the power supply output current.  
        If the persistent switch heater is off, the magnet current will be the value of the power supply output current when the persistent switch heater was last turned off.  
        The magnet current  will  be  set  to  zero  if  the  power  supply  detects  a  quench.    
        If  in  SHIM  mode,  the  IMAG? query reports the present current of the shim selected by the SHIM command in Amps.  
        If the optional Shim ID is provided while in shim mode, the present current of the specified shim will be reported. 
        Related Commands:   UNITS, UNITS?
        '''

        response = self.WriteString('IMAG?')

        # parse response
        # TODO: check handling on a bad readback
        return float(response.split(' ',1)[0])


    def QueryOutputCurrent(self):

        '''

        IOUT?
        Query power supply output current
        Availability:   Always                   
        Command Syntax:       IOUT?       
        Response:<Output Current> <Units> Response Example: 87.935 A
        Description:  The IOUT? query returns the power supply output current (or magnetic field strength) in the present units.  
        Related Commands:   UNITS, UNITS?

        '''
        response = self.WriteString('IOUT?')

        # parse response
        # TODO: check handling on a bad readback
        return float(response.split(' ',1)[0])


    def SetCurrentSweepLowerLimit(self,value):


        '''
        LLIM
        Set current sweep lower limit Availability:                   Remote                   Mode                   
        Command Syntax:       LLIM       [Limit]
        Example:                       LLIM                       20.1250                       
        Default Parameter:       0.0       
        Parameter  Range:+/-Maximum  Magnet  Current
        Description: The LLIM  command  sets  the  current  limit  used  when  the  next  SWEEP  DOWN  command is issued.  The value must be supplied in the selected units - amperes or field (kG).  An error will be returned if this value is greater than the upper sweep limit. 
        Related Commands:    LLIM?, ULIM, ULIM?, SWEEP, SWEEP?, UNITS, UNITS? 
        '''

        #TODO: handle field/current mode correctly. This naively assumes the incoming value is in the same unit system as the selected mode.

        boundinputvalue = self.BoundInput(value,-self.maxcurrent,self.maxcurrent)

        self.WriteString('LLIM ' + str(boundinputvalue))
        

    def QueryCurrentSweepLowerLimit(self):

        '''

        LLIM?
        Query current sweep lower limit 
        Availability:                   Always  
        Command Syntax:       LLIM?       
        Response:                    <Limit>                    <Units>                    
        Response Example: 20.1250 A Response Range:  +/-Maximum Magnet 
        Current Description:  The LLIM? query returns the current limit used with the SWEEP DOWN command.  It is issued in the selected units - amperes or field (kG). 
        Related Commands:    LLIM, ULIM, ULIM?, SWEEP, SWEEP?, UNITS, UNITS? 

        '''
        response = self.WriteString('LLIM?')

        # parse response
        # TODO: check handling on a bad readback
        return float(response.split(' ',1)[0])
        

    def SetLocalMode(self):

        '''
        LOCAL 
        Return control to front panel 
        Availability:Always (USB and Ethernet Only) 
        Command Syntax:       LOCAL       
        Description:    The LOCAL command  returns  control  the  front  panel  keypad  after  remote  control  has been selected by the REMOTE or RWLOCK commands. 
        Related Commands:   REMOTE, RWLOCK 
        '''

        self.WriteString('LOCAL')
        

    def QueryOperatingMode(self):

        '''
        MODE?
        Query selected operating mode
        Availability:                   Always 
        Command Syntax:       MODE?       
        Response:                    <Operating                    Mode>                    
        Response Example: Manual 
        Response Range:  Shim or Manual Description:  The MODE? command returns the present operating mode.   
        Related Commands:  MODE 

        '''

        response = self.WriteString('MODE?')

        # parse response
        # TODO: check handling on a bad readback
        return response.split(' ',1)[0]
        

    def SetName(self,value):


        '''
        NAME 
        Sets the name of the coil (magnet) on the display. 
        Availability:                   Remote                   Mode                   
        Command Syntax: NAME <name string> 
        Example:NAME GUN COIL 
        Default Parameter:       None       
        Parameter Range:  0 to 16 characters Description: The NAME  command  sets  the  name  of  the  currently  selected  module  for  display.    Upper and lower case is accepted, however the string is converted to upper case.
        Related Commands:    NAME?
        '''
        self.WriteString('NAME ' + str(value))
        
        # NAME
        

    def QueryName(self):

        '''
        NAME?
        Query the name of the currently selected coil (magnet)
        Availability:                   Always
        Command Syntax:       NAME?       
        Response:0 to 16 characters 
        Description:  The NAME? query returns the name of the currently selected module. 
        Related Commands:    NAME
        '''
        response = self.WriteString('NAME?')

        # parse response
        # TODO: check handling on a bad readback
        return response

    def ControlPersistentSwitchHeater(self,value):

        '''
        PSHTR 
        Control persistent switch heater
        Availability:                   Remote                   Mode                   
        Command Syntax: PSHTR <State> 
        Example:                       PSHTR                       ON                       
        Default Parameter:       None       
        Parameter Range:  On or Off 
        Description: The PSHTR  command  turns  the  persistent  switch  heater  on  or  off.    Note  that  the  switch heater current can only be set in the Magnet Menu using the keypad.  This command should normally  be  used  only  when  the  supply  output  is  stable  and  matched  to  the  magnet  current.    If  in  Shim Mode, the heater for the selected shim is controlled instead of the main switch heater.
        Related Commands:    PSHTR?
        '''

        if value:
            value = 'ON'
        else:
            value = 'OFF'

        self.WriteString('PSHTR ' + str(value))
        

    def QueryPersistentSwitchHeaterState(self):

        '''
        PSHTR?
        Query persistent switch heater state 
        Availability:                   Always                   
        Command Syntax:       PSHTR?       
        Response:0 or 1 
        Description:    The PSHTR? query  returns  1  if  the  switch  heater  is  ON  or  0  if  the  switch  heater  is  OFF.  If in Shim Mode, the status of the switch heater for the selected shim is returned. 
        Related Commands:    PSHTR QRESET Reset Quench Condition

        '''
        response = self.WriteString('PSHTR?')

        # parse response
        # TODO: check handling on a bad readback
        return bool(int(response.split(' ',1)[0]))
        

    def ResetQuenchCondition(self):
        
        '''
        QRESET 
        Reset Quench Condition
        CAvailability:                   Remote                   Mode                   
        Command Syntax: QRESET 
        Description: The QRESET  command  resets  a  power  supply  quench  condition  and  returns  the  supply to STANDBY.
        Related Commands:    None
        '''
        self.WriteString('QRESET')
        

    def SetRangeLimit(self,rangeselect,value):
        '''
        RANGE
        Set range limit for sweep rate boundary 
        Availability:                   Remote                   
        Command Syntax:RANGE <Select> <Limit> 
        Example:RANGE 0 25.0 
        Default Parameter:       None       
        Parameter Ranges:
        Range Selection:   0 to 4 
        Limit:   0 to Max Supply Current
        '''
        boundrangevalue = self.IntegerCasting(self.BoundInput(rangeselect,0,4),1)
        boundinputvalue = self.BoundInput(value,-self.maxcurrent,self.maxcurrent)
        self.WriteString('RANGE ' + str(boundrangevalue)+ ' ' + str(boundinputvalue))


    def QueryRangeLimit(self,rangeselect):

        '''
        RANGE?
        Query range limit for sweep rate boundary 
        Availability:                   Always                   
        Command Syntax:       RANGE?       <Select>       
        Example:                       RANGE?                       1                                              
        Parameter Range: 0 to 4 
        Response:                    <Limit>                    
        Response Example: 75.000 
        Response Range: 0 to Max Magnet Current 
        Description:    The RANGE? query  returns  the  upper  limit  for  a  charge  rate  range  in  amps.    See  RANGE for further details. 
        Related Commands:   RANGE, RATE, RATE? 
        '''
        boundrangevalue = self.IntegerCasting(self.BoundInput(rangeselect,0,4),1)
        response = self.WriteString('RANGE? ' + str(boundrangevalue))

        # parse response
        # TODO: check handling on a bad readback
        return response.split(' ',1)[0]
        

    def SetSweepRate(self,rangeselect,value):

        '''
        RATE
        Set sweep rate for selected sweep range 
        Availability:                   Remote                   Command 
        Syntax:RATE <Range> <Sweep Rate> 
        Example:RATE 0 0.250 
        Default Parameter:       None       
        Parameter Ranges:
        Range Selection:  0 to 5 
        Limit:     0 to Max Magnet Current 
        Description: The RATE command  sets  the  charge  rate  in  amps/second  for  a  selected  range.    
        A  range parameter of 0, 1, 2, 3, and 4 will select Range 1, 2, 3, 4, or 5 sweep rates as displayed in the Rates Menu.  
        A range parameter of 5 selects the Fast mode sweep rate. 
        Related Commands:   RANGE, RANGE?, RATE?
        '''
     
        boundrangevalue = self.IntegerCasting(self.BoundInput(rangeselect,0,5),1)
        boundinputvalue = self.BoundInput(value,-self.maxcurrent,self.maxcurrent)
        self.WriteString('RATE ' + str(boundrangevalue)+ ' ' + str(boundinputvalue))

    def QuerySweepRate(self, rangeselect):


        '''
        RATE?
        Query range limit for sweep rate boundary 
        Availability:                   Always                   
        Command Syntax:       RATE?       <Range>       
        Example:                       RATE?                       1                                              
        Parameter Range: 0 to 5 
        Response:                    <Rate>                    
        Response Example: 0.125 
        Response Range: 0 to Max Magnet Current 
        Description: The RATE? command queries the charge rate in amps/second for a selected range.  
        A  range  parameter  of  0  to  4  will  select  Range  1  through  5  sweep  rates  as  displayed  in  the  Rates  Menu.  
        A range parameter of 5 queries the Fast mode sweep rate. 
        Related Commands:   RANGE, RANGE?, RATE
        '''

        boundrangevalue = self.IntegerCasting(self.BoundInput(rangeselect,0,5),1)
        response = self.WriteString('RATE? ' + str(boundrangevalue))

        # parse response
        # TODO: check handling on a bad readback
        return response.split(' ',1)[0]
        

    def SetRemoteMode(self):

        '''
        REMOTE
        Select remote operation
        Availability: Operate (USB Only) 
        Command Syntax:       REMOTE       
        Description:  The REMOTE command takes control of the 4G via the remote interface.  All buttons on the front panel are disabled except the Local button.  
        This command will be rejected if the menu system is being accessed via the front panel or if LOCAL has been selected via the Local button on the  front  panel.    
        Pressing  the  Local  button  again  when  the  menu  is  not  selected  will  allow  this  command to be executed.  
        This command is only necessary for USB operation since the IEEE-488 interface provides for bus level control of the Remote and Lock controls. 
        Related Commands:   LOCAL, RWLOCK
        '''

        self.WriteString('REMOTE')
        

    def SetRemoteWithLockMode(self):

        '''
        RWLOCK
        Select remote operation
        Availability:Operate (USB Only) 
        Command Syntax:       RWLOCK      
        Description:  The REMOTE command takes control of the 4G via the remote interface.  All buttons on the front panel are disabled except the Local button.  
        This command will be rejected if the menu system is being accessed via the front panel or if LOCAL has been selected via the Local button on the  front  panel.    
        Pressing  the  Local  button  again  when  the  menu  is  not  selected  will  allow  this  command to be executed.  
        This command is only necessary for USB operation since the IEEE-488 interface provides for bus level control of the Remote and Lock controls. 
        Related Commands:   LOCAL, RWLOCK 
        '''
        
        self.WriteString('RWLOCK')

    def SelectShim(self,value):


        # SHIM
        pass

    def QueryShimSelected(self):


        response = self.WriteString('SHIM?')

        # parse response
        # TODO: check handling on a bad readback
        return response.split(' ',1)[0]

    def SetShimCurrentLimit(self,value):

        
        # SLIM
        pass

    def QueryShimCurrentLimit(self):

        response = self.WriteString('SLIM?')

        # parse response
        # TODO: check handling on a bad readback
        return response.split(' ',1)[0]

    def StartOutputCurrentSweep(self,value):

        '''
        SWEEP
        Start output current sweep 
        Availability:                   Remote                   Mode                   
        Command Syntax: SWEEP <Sweep Mode> [fast or slow] 
        Examples:                     SWEEP                     UP                     
        SWEEP UP FAST 
        Default Parameter: None 
        Parameter Range:  UP, DOWN, PAUSE, or ZERO 
        Shim Mode Only: LIMIT
        Description:    The SWEEP command  causes  the  power  supply  to  sweep  the  output  current  from  the  present  current  to  the  specified  limit  at  the  applicable  charge  rate  set  by  the  range  and  rate  commands. If  the  FAST  parameter  is  given,  the  fast  mode  rate  will  be  used  instead  of  a  rate  selected from the output current range.  SLOW is required to change from fast sweep.  SWEEP UP sweeps  to  the  Upper  limit,  SWEEP  DOWN  sweeps  to  the  Lower  limit,  and  SWEEP  ZERO  discharges the supply.  If in Shim Mode, SWEEP LIMIT sweeps to the shim target current. 
        Related Commands:   LLIM, LLIM?, SLIM, SLIM?, SWEEP?, ULIM, ULIM?, UNITS, UNITS?
        '''

        self.WriteString('SWEEP '+ str(value))
        

    def QuerySweepMode(self):

        '''
        SWEEP?
        Query sweep mode 
        Availability:                   Always                   
        Command Syntax:       SWEEP?       
        Response:                    <Mode>                    [fast]                    
        Response Example: sweep up fast Response  Range:    sweep  up,  sweep  down,  sweep paused, or zeroing 
        Description:    The SWEEP? query  returns  the  present  sweep  mode.    If  sweep  is  not  active  then  'sweep paused' is returned. 
        Related Commands:   LLIM, LLIM?, SLIM, SLIM?, SWEEP, ULIM, ULIM?, UNITS, UNITS?
        '''


        response = self.WriteString('SWEEP?')

        # parse response
        # TODO: check handling on a bad readback
        if(len(response.split(' ',1)))==1:
            return(response)
        else:
            return response.split(' ',1)[1]
        

    def SetCurrentSweepUpperLimit(self,value):
        '''
        ULIM
        Set current sweep upper limit 
        Availability:                   Remote                   Mode                   
        Command Syntax:       ULIM       [Limit]       
        Example:                       ULIM                       65.327                       
        Default Parameter:       0.0       Parameter  Range:+/-Maximum  Supply  Current
        Description: The ULIM command sets the current limit used when the next SWEEP UP command is issued.  The value must be supplied in the selected units - amperes or field (kG).  An error will be returned if this value is less than the lower sweep limit. 
        Related Commands:   LLIM, LLIM?, SWEEP, SWEEP?, ULIM?, UNITS, UNITS?
        '''

        boundinputvalue = self.BoundInput(value,-self.maxcurrent,self.maxcurrent)

        self.WriteString('ULIM ' + str(boundinputvalue))
        

    def QueryCurrentSweepUpperLimit(self):

        '''
        ULIM?
        Query current sweep upper limit 
        Availability:                   Always                   
        Command Syntax:       ULIM?       
        Response:                    <Limit>                    <Units>                    
        Response Example: 65.327 A 
        Response Range:  +/-Maximum              Supply              
        Current Description:  The ULIM? query returns the current limit used for the SWEEP UP command.  It is issued in the selected units - amperes or field (kG). 
        Related Commands:   LLIM, LLIM?, SWEEP, SWEEP?, ULIM, UNITS, UNITS?
        '''


        response = self.WriteString('ULIM?')

        # parse response
        # TODO: check handling on a bad readback
        return float(response.split(' ',1)[0])
        

    def SetUnits(self,value='A'):
        '''
        UNITS                            
        Select                            units                            
        Availability:                   Remote                   Mode                   
        Command Syntax:UNITS <Unit Selection> 
        Example:                       UNITS                       A                                                                     
        Parameter Range:  A, G Description:  The UNITS command sets the units to be used for all input and display operations.  Units may be set to Amps or Gauss.  The unit will autorange to display Gauss, Kilogauss or Tesla. 
        Related Commands:  IMAG?, IOUT?, LLIM, LLIM?, ULIM, ULIM?, UNITS?
        '''
        self.WriteString('UNITS '+str(value))
        

    def QueryUnits(self):


        '''
        UNITS?
        Query selected units 
        vailability:                   Always                   
        Command Syntax:       UNITS?       
        Response:                    <Selected                    Units>                    
        Response Example: G Response Range: A, G 
        Description:  The UNITS? command returns the units used for all input and display operations.  
        Related Commands:  IMAG?, IOUT?, LLIM, LLIM?, ULIM, ULIM?, UNITS 
        '''

        response = self.WriteString('UNITS?')

        # parse response
        # TODO: check handling on a bad readback
        return response.split(' ',1)[0]
        

    def SetVoltageLimit(self,value):


        '''
        VLIM 
        Set voltage limit 
        Availability:                   Remote                   Mode                  
        Command Syntax:VLIM <Voltage Limit> Example:                       VLIM                       5.0
        Parameter Range:  0.0 to 10.0 
        Description:    The VLIM  command  sets  the  power  supply  output  voltage  limit  to  the  voltage  provided.  
        Related Commands: VLIM?, VMAG?, VOUT? 
        '''

        boundinputvalue = self.BoundInput(value,0,10.0)

        self.WriteString('VLIM ' + str(boundinputvalue))
        

    def QueryVoltageLimit(self):

        '''
        VLIM?
        Query voltage limit Availability:                   Always                  
        Command Syntax:       VLIM?       
        Response:                    <Voltage                    Limit>                    
        Response Example: 4.75 V 
        Response Range: 0 to 10.00 
        Description:  The VLIM? command returns the power supply output voltage limit.  
        Related Commands: VLIM, VMAG?, VOUT?
        '''

        response = self.WriteString('VLIM?')

        # parse response
        # TODO: check handling on a bad readback
        return float(response.split(' ',1)[0])

    def QueryMagnetVoltage(self):


        '''
        VMAG?
        Query magnet voltage 
        Availability:                   Always                   
        Command Syntax:       VMAG?       
        Response:                    <Magnet                    Voltage>                    
        Response Example: 4.75 V Response Range: -10.00 to +10.00 
        Description:  The VMAG? command returns the present magnet voltage.  
        Related Commands: VLIM, VLIM?, VOUT?
        '''

        response = self.WriteString('VMAG?')

        # parse response
        # TODO: check handling on a bad readback
        return float(response.split(' ',1)[0])
        

    def QueryOutputVoltage(self):

        '''
        VOUT?Query output voltage 
        Availability:                   Always                   
        Command Syntax:       VOUT?       
        Response:                    <Output                    Voltage>                    
        Response Example: 4.75 V Response Range: -12.80 to +12.80 
        Description:  The VOUT? command returns the present power supply output voltage.  
        Related Commands:  VLIM, VLIM?, VMAG? 
        '''

        response = self.WriteString('VOUT?')

        # parse response
        # TODO: check handling on a bad readback
        return float(response.split(' ',1)[0])

# The following appears to not be accessible via Ethernet and are included only for completion.

    def ClearStatusCommand(self):


        #*CLS
        pass

    def CommandStandardEventStatusEnable(self,value):


        #*ESE
        pass

    def QueryStandardEventStatusEnable(self):

        response = self.WriteString('*ESE?')

        # parse response
        # TODO: check handling on a bad readback
        return response.split(' ',1)[0]

    def QueryStandardEventStatusRegister(self):

        response = self.WriteString('*ESR?')

        # parse response
        # TODO: check handling on a bad readback
        return response.split(' ',1)[0]

    def QueryIdentification(self):

        response = self.WriteString('*IDR?')

        # parse response
        # TODO: check handling on a bad readback
        return response.split(' ',1)[0]

    def CommandOperationComplete(self):


        #*OPC
        pass

    def QueryOperationComplete(self):

        response = self.WriteString('*OPC?')

        # parse response
        # TODO: check handling on a bad readback
        return response.split(' ',1)[0]

    def CommandReset(self):

        response = self.WriteString('*RST')

        # parse response
        # TODO: check handling on a bad readback
        return response.split(' ',1)[0]

    def CommandServiceRequestEnable(self):


        #*SRE
        pass

    def QueryServiceRequestEnable(self):

        response = self.WriteString('*SRE?')

        # parse response
        # TODO: check handling on a bad readback
        return response.split(' ',1)[0]

    def QueryReadStatusByte(self):

        response = self.WriteString('*STB?')

        # parse response
        # TODO: check handling on a bad readback
        return response.split(' ',1)[0]

    def QuerySelfTest(self):

        response = self.WriteString('*TST?')

        # parse response
        # TODO: check handling on a bad readback
        return response.split(' ',1)[0]

    def CommandWaitToContinue(self):


        #*WAI
        pass

    
            

        


